Extra Theme Variables Addon
================================

Neuhub themes can be customized in a number of ways. For example, you can set the home page for the entire website, and change the site-wide navigation. You also can enabled and disable features.

Full Documentation (including example HTML) is located at: [https://neuhub.org/page/documentation/extra-theme-variables](https://neuhub.org/page/documentation/extra-theme-variables)

### Home Path 

Name: `home_path`

You can change the destination of the "Home" page link in the navigation bar by setting this variable.

### Custom HTML Above Sidebar Menu

Name: `sidebar_above_menu`

You can add any custom HTML above the sidebar menu. On this website we chose to include our logo.

### Add Additional Menu Items in Sidebar

Name: `sidebar_li_after_home`

You can add additional navigation items in the sidebar. These will appear after the Home link and before the User Navigation (that only appears to logged in users).

We used Tabler icons for the sidebar, so it is recommended that you use them as well so your icons match ours. You can select from over 5000 icons at: https://tabler.io/icons

