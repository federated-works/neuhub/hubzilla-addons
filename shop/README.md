**Before using the addon**, you must [**configure its settings**](shop_settings).

Also, note the following:

- This addon will **not** work without the "custompage" addon.
- Only PayPal is currently supported.